# MovieBox - Android App

This is and Android Test implementation for a movie related mobile applications.

## Functionalities

I started the project from scratch with the following functionalities:

List horizontally currently playing movies.
Display the most popular movies in the vertical list view, as this list will contain multiple pages, Pagination support will be required.
When a user clicks on any movie list item, it will navigate to a detailed screen, with more information about the movie.
Detail screen with movie information.

## Features

The android app lets you:
- See movies playing now
- See catalog of movies
- Details of any movie
- Pagination

## Screenshots
    
![Scheme](readme/AndroidTest.jpg)
![Scheme](readme/AndroidTest2.jpg)
    
## Permissions

On Android versions prior to Android 5.0, moviebox requires the following permissions:
- Full Network Access.

## Libraries
- Retrofit(Networking)
- Picasso(Images)

## License

This application is released under GNU GPLv3 (see [LICENSE](LICENSE)).
Some of the used libraries are released under different licenses.