package com.example.androidtest;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.androidtest.Models.Genre;
import com.example.androidtest.Models.Movie;
import com.example.androidtest.Models.PlayingMovie;
import com.example.androidtest.Networking.Api;
import com.example.androidtest.Networking.RetrofitClient;
import com.example.androidtest.Utils.Constants;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieFragment extends DialogFragment {

    private ImageView movieIv;
    ImageButton backIb;
    private TextView movieNameTv, movieInfoTv, movieOverviewTv;
    private PlayingMovie movie;
    private ChipGroup chipGroup;
    private List<Genre> genres;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_movie, container, false);

        movieIv = view.findViewById(R.id.movie_poster_iv);
        movieNameTv = view.findViewById(R.id.movie_name_tv);
        movieInfoTv = view.findViewById(R.id.info_tv);
        movieOverviewTv = view.findViewById(R.id.overview_tv);
        backIb = view.findViewById(R.id.back_ib);
        chipGroup = view.findViewById(R.id.chip_group);

        backIb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        setData();
        getMovie();

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            movie = (PlayingMovie) getArguments().getSerializable("Movie");
        }
    }

    private void setData(){
        movieNameTv.setText(movie.getTitle());
        movieInfoTv.setText(movie.getReleaseDate());
        movieOverviewTv.setText(movie.getOverview());
        Picasso.get().load(movie.getPosterPath()).into(movieIv);
    }

    private void setTags(){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        for(Genre genre: genres){
            Chip chip = (Chip) inflater.inflate(R.layout.chip_item, null, false);
            chip.setText(genre.getName());
            chipGroup.addView(chip);

        }
    }

    public void getMovie(){

        Api service = RetrofitClient.getRetrofit().create(Api.class);
        Call<Movie> call = service.getMovie(movie.getId(), Constants.API_KEY);
        call.enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        if (response.body().getGenres().size() != 0) {
                            genres = response.body().getGenres();
                            setTags();
                        }
                    }else{
                        System.out.println("error");
                    }
                }else{
                    System.out.println("error");
                }
            }

            @Override
            public void onFailure(Call<Movie> call, Throwable t) {
                System.out.println("error");
            }
        });
    }
}