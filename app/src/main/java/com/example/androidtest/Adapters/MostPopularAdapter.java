package com.example.androidtest.Adapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidtest.Models.PlayingMovie;
import com.example.androidtest.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MostPopularAdapter extends RecyclerView.Adapter<MostPopularAdapter.ViewHolder> {

    private List<PlayingMovie> mostPopularMovies;
    private MostPopularAdapter.ListItemClickListener mClick;

    public interface ListItemClickListener {
        void onListItemClick(int clickedItemIndex);
    }

    public void setData(List<PlayingMovie> mostPopularMovies) {this.mostPopularMovies = mostPopularMovies; }


    public MostPopularAdapter (MostPopularAdapter.ListItemClickListener listener){
        this.mClick = listener;
    }
    @NonNull
    @Override
    public MostPopularAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.popular_item_layout, viewGroup, false);
        return new MostPopularAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MostPopularAdapter.ViewHolder holder, int i) {

        final PlayingMovie playingMovie = mostPopularMovies.get(i);
        Context context = holder.itemView.getContext();

        if(playingMovie.getPosterPath() != null && !playingMovie.getPosterPath().isEmpty() ){
            Picasso.get().load(playingMovie.getPosterPath()).into(holder.imageView);
        }

        String movieName = playingMovie.getTitle();
        String releaseDate = playingMovie.getReleaseDate();
        String duration = "3h 1m";
        double average = playingMovie.getVoteAverage();
        String percentage = playingMovie.getVoteAverage() + "%";
        int progress = (int) Math.round(average);

        holder.movieNameTv.setText(movieName);
        holder.realeaseDateTv.setText(releaseDate);
        holder.durationTv.setText(duration);
        holder.percentageTv.setText(percentage);
        holder.progressBar.setProgress(progress);
        holder.progressBar.setMax(100);

        if (average > 69){
            holder.progressBar.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorGreen)));
        }else if (average >= 40 && average < 70){
            holder.progressBar.setProgressBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorAccent)));
        }else{
            holder.progressBar.setProgressBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorRed)));
        }

    }

    @Override
    public int getItemCount() {
        return mostPopularMovies.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView imageView;
        private TextView movieNameTv, realeaseDateTv, durationTv, percentageTv;
        private ProgressBar progressBar;


        private ViewHolder(View view){
            super(view);
            view.setOnClickListener(this);
            imageView = view.findViewById(R.id.item_image_view);
            movieNameTv = view.findViewById(R.id.movie_name_tv);
            realeaseDateTv = view.findViewById(R.id.date_tv);
            durationTv = view.findViewById(R.id.duration_tv);
            percentageTv = view.findViewById(R.id.percentage_tv);
            progressBar = view.findViewById(R.id.progress_bar);

        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            mClick.onListItemClick(position);
        }
    }
}
