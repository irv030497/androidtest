package com.example.androidtest.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidtest.Models.PlayingMovie;
import com.example.androidtest.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PlayingMovieAdapter extends RecyclerView.Adapter<PlayingMovieAdapter.ViewHolder>{

    private List<PlayingMovie> playingMovies;

    public void setData(List<PlayingMovie> playingMovies) {this.playingMovies = playingMovies; }

    @NonNull
    @Override
    public PlayingMovieAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.image_item_layout, viewGroup, false);
        return new PlayingMovieAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PlayingMovieAdapter.ViewHolder holder, int i) {

        final PlayingMovie playingMovie = playingMovies.get(i);

        if(playingMovie.getPosterPath() != null && !playingMovie.getPosterPath().isEmpty() ){
            Picasso.get().load(playingMovie.getPosterPath()).into(holder.imageView);
        }
    }

    @Override
    public int getItemCount() {
        return playingMovies.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;

        private ViewHolder(View view){
            super(view);
            imageView = view.findViewById(R.id.movie_iv);
        }

    }
}
