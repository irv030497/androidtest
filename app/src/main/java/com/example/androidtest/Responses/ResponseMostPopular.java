package com.example.androidtest.Responses;

import com.example.androidtest.Models.PlayingMovie;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseMostPopular {

    @SerializedName("total_pages")
    private int totalPages;
    private List<PlayingMovie> results;

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public List<PlayingMovie> getResults() {
        return results;
    }

    public void setResults(List<PlayingMovie> results) {
        this.results = results;
    }
}
