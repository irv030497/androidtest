package com.example.androidtest.Responses;

import com.example.androidtest.Models.PlayingMovie;

import java.util.List;

public class ResponsePlayingMovie {

    private List<PlayingMovie> results = null;

    public List<PlayingMovie> getResults() {
        return results;
    }

    public void setResults(List<PlayingMovie> results) {
        this.results = results;
    }
}
