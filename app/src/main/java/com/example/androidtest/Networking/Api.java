package com.example.androidtest.Networking;

import com.example.androidtest.Models.Movie;
import com.example.androidtest.Responses.ResponsePlayingMovie;
import com.example.androidtest.Responses.ResponseMostPopular;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Api {

    @GET("3/movie/now_playing")
    Call<ResponsePlayingMovie> getPlayingMovies(@Query("language") String language, @Query("page") String page, @Query("api_key") String apiKey);

    @GET("3/movie/popular")
    Call<ResponseMostPopular> getPopularMovies(@Query("api_key") String apiKey, @Query("language") String language,
                                               @Query("page") int page);

    @GET("3/movie/{movie_id}")
    Call<Movie> getMovie(@Path("movie_id") int movieId, @Query("api_key") String apiKey);
}
