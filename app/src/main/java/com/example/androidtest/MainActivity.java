package com.example.androidtest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.androidtest.Adapters.MostPopularAdapter;
import com.example.androidtest.Adapters.PlayingMovieAdapter;
import com.example.androidtest.Models.PlayingMovie;
import com.example.androidtest.Responses.ResponsePlayingMovie;
import com.example.androidtest.Responses.ResponseMostPopular;
import com.example.androidtest.Networking.Api;
import com.example.androidtest.Networking.RetrofitClient;
import com.example.androidtest.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements MostPopularAdapter.ListItemClickListener{

    private RecyclerView imagesRV, moviesRV;
    private Button seeMoreBtn;
    private List<PlayingMovie> playingMovies;
    private List<PlayingMovie> mostPopularMovies;
    PlayingMovieAdapter playingMovieAdapter;
    MostPopularAdapter mostPopularAdapter;
    private int page;
    private int totalPages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imagesRV = findViewById(R.id.images_recyclerview);
        moviesRV = findViewById(R.id.movies_recyclerview);
        seeMoreBtn = findViewById(R.id.see_more_btn);

        seeMoreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMoreMovies();
            }
        });

        mostPopularMovies = new ArrayList<>();
        page = 1;

        getPlayingMovies();
        getPopularMovies(page);
    }

    private void setImagesAdapter(List<PlayingMovie> playingMovies){
        playingMovieAdapter = new PlayingMovieAdapter();
        playingMovieAdapter.setData(playingMovies);
        imagesRV.setAdapter(playingMovieAdapter);
        imagesRV.setHasFixedSize(true);
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        imagesRV.setLayoutManager(horizontalLayoutManagaer);
    }

    private void setMostPopularAdapter(List<PlayingMovie> playingMovies){
        mostPopularAdapter = new MostPopularAdapter(this);
        mostPopularAdapter.setData(playingMovies);
        moviesRV.setAdapter(mostPopularAdapter);
        moviesRV.setHasFixedSize(true);

        DividerItemDecoration horizontalDecoration = new DividerItemDecoration(moviesRV.getContext(),
                DividerItemDecoration.VERTICAL);
        Drawable horizontalDivider = ContextCompat.getDrawable(this, R.drawable.vertical_divider);
        horizontalDecoration.setDrawable(horizontalDivider);
        moviesRV.addItemDecoration(horizontalDecoration);
        moviesRV.setLayoutManager(new LinearLayoutManager(this));
    }

    private void getMoreMovies(){
        if(page + 1 == totalPages){
            seeMoreBtn.setVisibility(View.GONE);
        }
        page += 1;
        if(page<=totalPages) {
            getPopularMovies(page);
        }
    }

    public void getPlayingMovies(){

        Api service = RetrofitClient.getRetrofit().create(Api.class);
        String page = "undefined";
        Call<ResponsePlayingMovie> call = service.getPlayingMovies(Constants.LANGUAGE, page, Constants.API_KEY);
        call.enqueue(new Callback<ResponsePlayingMovie>() {
            @Override
            public void onResponse(Call<ResponsePlayingMovie> call, Response<ResponsePlayingMovie> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        if (response.body().getResults().size() != 0) {
                            playingMovies = response.body().getResults();
                            setImagesAdapter(playingMovies);
                        }
                    }else{
                        System.out.println("error");
                    }
                }else{
                    System.out.println("error");
                }
            }

            @Override
            public void onFailure(Call<ResponsePlayingMovie> call, Throwable t) {
                System.out.println("error");
            }

        });
    }

    public void getPopularMovies(int page){

        Api service = RetrofitClient.getRetrofit().create(Api.class);
        Call<ResponseMostPopular> call = service.getPopularMovies(Constants.API_KEY, Constants.LANGUAGE, page);
        call.enqueue(new Callback<ResponseMostPopular>() {
            @Override
            public void onResponse(Call<ResponseMostPopular> call, Response<ResponseMostPopular> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        if (response.body().getResults().size() != 0) {
                            totalPages = response.body().getTotalPages();
                            mostPopularMovies.addAll(response.body().getResults());
                            setMostPopularAdapter(mostPopularMovies);
                        }
                    }else{
                        System.out.println("error");
                    }
                }else{
                    System.out.println("error");
                }
            }

            @Override
            public void onFailure(Call<ResponseMostPopular> call, Throwable t) {
                System.out.println("error");
            }

        });
    }

    @Override
    public void onListItemClick(int clickedItemIndex) {
        MovieFragment movieFragment = new MovieFragment();
        movieFragment.setCancelable(false);
        Bundle args = new Bundle();
        args.putSerializable("Movie", mostPopularMovies.get(clickedItemIndex));
        movieFragment.setArguments(args);
        movieFragment.show(getSupportFragmentManager(), "MovieFragment");
    }
}